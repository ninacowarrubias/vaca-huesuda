require "sinatra"

module ProjectNameGoesHere
  class Application < Sinatra::Base
    include VacaHuesuda

    get "/" do
      @nombre = Time.now.to_i
      erb :index
    end

    get "/items" do
      erb :items
    end
  end
end
