module VacaHuesuda
  def self.included(base)
    base.instance_exec do
      set :public_folder, File.expand_path("../public", File.dirname(__FILE__))
      helpers Helpers
    end
  end

  module Helpers
    def meta(key, value = nil)
      @meta ||= {}
      @meta[key] = value unless value.nil?
      @meta[key]
    end

    def render_menu_item(title, path)
      %{<li class="#{"active" if current_menu_is?(path)}"><a href="#{path}">#{title}</a></li>}
    end

    def current_menu_is?(nav_id_or_path)
      meta(:navigation) && nav_id_or_path.to_s.match(/^#{meta(:navigation)}/)
    end
  end
end
